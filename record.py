import sqlite3
import base64
import sys
import cv2
from colorama import init,  Fore, Back, Style
from secret import PASSWORD

init()


connect = input("Cual es tu password ?\n")


while connect != PASSWORD:
    connect = input("Cual es tu password ?\n")
    if connect == "q":
        break


conn = sqlite3.connect("safe.db")


try:
    conn.execute(
        '''CREATE TABLE SAFEST
        (SERVICE TEXT PRIMARY KEY NOT NULL,
        USERNAME TEXT NOT NULL,
        PASSWORD TEXT NOT NULL)'''
    )
    print('\nTu seguridad ha sido creada.\nQue te gustaría guardar hoy?')
except: 
        print('\nYa estás seguro.\nQue te gustaría hacer hoy?\n')


while True:
    print("*"*15)
    print("COMMANDS: ")
    print("q = Terminar programa")
    print("o = Abrir")
    print("s = Store")
    print("*"*15)
    _input = input(":").lower()

    if(_input == "q"):
        break
    ## Open password 
    if(_input == "o"):
        serviceName = input("\nCual es el nombre del servicio que desea?\n\n").lower()
        cursor = conn.execute("SELECT * FROM SAFEST WHERE SERVICE=" + '"' + serviceName + '"')   
        for row in cursor:
            print(Fore.GREEN + str(row))
            print(Style.RESET_ALL)

    if(_input == "s"):
        serviceName = input("Cual es el nombre del servicio que desea?\n\n").lower()
        userName = input("Cual es el username de su cuenta?\n\n").lower()
        passwordName = input("Cual es el password de su cuenta?\n\n").lower()

        conn.execute("INSERT INTO SAFEST(SERVICE, USERNAME, PASSWORD) VALUES (?, ?, ?)",(serviceName, userName, passwordName))

        conn.commit()

